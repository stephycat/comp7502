/**

In this you task will implement the method getMostFrequentIntensityValue that will return the most frequent intensity value in the image, i.e., the intensity that appears most often. 

Your program should output the following.

Most frequent intensity value is 194

**/

import java.util.Scanner;

public class Lab1_4 {
  public Lab1_4() {
    Img img = new Img("Fig0314a.png");
    int i = getMostFrequentIntensityValue(img);
    System.out.println("Most frequent intensity value is "+i);
  }
  /**
   * Retrieve the intensity value that occurs most often in the image
   * @param img 
   * @return the intensity value that occurs most often in the image
   */
  public int getMostFrequentIntensityValue(Img i) {
    // Your code here
    int[] intensity = new int[256];
    int maxIntensityCount = 0;
    int maxIntensityValue = 0;

    for (int x=0; x<i.height; x++) {
      for (int y=0; y<i.width; y++) {
        intensity[i.img[x*i.width+y] & 0XFF]++;
      }
    }

    for (int n=0; n<256; n++) {
      if (intensity[n] > maxIntensityCount) {
        maxIntensityValue = n;
        maxIntensityCount = intensity[n];
      }
    }

    return maxIntensityValue;
  }
  public static void main(String[] args) {
    new Lab1_4();
  }
}
