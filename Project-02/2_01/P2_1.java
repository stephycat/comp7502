/**
 * In this task you will implement the method gradientImage of the class P2_1 which will calculate the gradient image.
 *
 * To determine the gradient in images in x and y directions use the masks [-1 0 1]^T and [-1 0 1], respectively.
 *
 * Note that values should be scaled to [0, 255]. This can be done by multiplying with 1 / sqrt(2). 
 *
 * The solution files are provided for qualitative comparison. Output could be different because of differences in floating point arithmetic.
 **/
public class P2_1 {
  public P2_1() {
    Img img = new Img("Fig0314a.png");
    gradientImage(img);
    img.save();
  }

  public void gradientImage(Img i) {
    // Your code here
    byte[] result = new byte[i.height*i.width];
    for (int x=1; x<i.height-1; x++) {
      for (int y=1; y<i.width-1; y++) {
        final int dxp = (x+1)*i.width+y, dxm = (x-1)*i.width+y;
        final int dx = (i.img[dxp] & 0xFF) - (i.img[dxm] & 0xFF);
        final int dyp = x*i.width+(y+1), dym = x*i.width+(y-1);
        final int dy = (i.img[dyp] & 0xFF) - (i.img[dym] & 0xFF);
        final double gradient = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2)) / Math.sqrt(2);
        result[x*i.width+y] = (byte)((int)gradient);
      }
    }
    i.img = result;
  }

  public static void main(String[] args) {
    new P2_1();
  }
}
