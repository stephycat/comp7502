/**
 * In this task you will implement the method cornerResponseImage of the class P2_3 which will change the image to the response map R of the Harris corner detector. As usual, ignore the boundary. 
 * 
 * Set pixels to 255 if R > threshold and otherwise set pixels to 0. 
 * 
 *  The solution files are provided for qualitative comparison. Output could be different because of differences in floating point arithmetic.
 **/
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

class Interim implements Cloneable {
  private int height;
  private int width;
  private double[] pixels;

  public Interim(int height, int width) {
    this.height = height;
    this.width = width;
    this.pixels = new double[height*width];
  }

  public int getHeight() {
    return this.height;
  }

  public int getWidth() {
    return this.width;
  }

  public double getPixel(int x, int y) {
    return this.pixels[x*width+y];
  }

  public void setPixel(int x, int y, double value) {
    this.pixels[x*width+y] = value;
  }
}

public class P2_3 {
  public P2_3() { 
    Img img = new Img("chessmat.png");
    System.out.print("Sigma: ");
    Scanner in = new Scanner(System.in);
    double s = in.nextDouble();
    System.out.print("Threshold: ");
    double t = in.nextDouble();
    cornerResponseImage(img, s, t);
    img.save();
  }

  public Interim gaussianSmooth(Interim data, double sigma) {
    // Your code here
    Interim result = new Interim(data.getHeight(), data.getWidth());
    final int radius = (int)Math.sqrt(-2*Math.pow(sigma,2)*Math.log(0.001));

    final double[] kernelRaw = IntStream
      .rangeClosed(-radius, radius)
      .mapToDouble(n -> Math.exp(-Math.pow(n,2)/(2*Math.pow(sigma,2)))/(Math.sqrt(2*Math.PI)*sigma))
      .toArray();
    final double kernelSum = DoubleStream.of(kernelRaw).sum();
    final double[] kernel = DoubleStream.of(kernelRaw).map(n -> n/kernelSum).toArray();

    for (int x: IntStream.range(radius, data.getHeight()-radius).toArray()) {
      for (int y: IntStream.range(radius, data.getWidth()-radius).toArray()) {
        double sum = 0.0;
        for (int u: IntStream.rangeClosed(-radius, radius).toArray())
          for (int v: IntStream.rangeClosed(-radius, radius).toArray())
            sum += kernel[u+radius]*kernel[v+radius]*data.getPixel(x+u, y+v);
        result.setPixel(x, y, sum);
      }
    }

    return result;
  }

  @SuppressWarnings("serial")
  public void cornerResponseImage(Img i, double sigma, double threshold) {
    // Your code here
    final double KAPPA = 0.04;

    ConcurrentHashMap<String, Interim> partiald = new ConcurrentHashMap<String, Interim>() {{
      put("fx2", new Interim(i.height, i.width));
      put("fy2", new Interim(i.height, i.width));
      put("fxy", new Interim(i.height, i.width));
    }};
    ConcurrentHashMap<String, Interim> gaussian = new ConcurrentHashMap<String, Interim>();

    for (int x: IntStream.range(1, i.height-1).toArray()) {
      for (int y: IntStream.range(1, i.width-1).toArray()) {
        final int dxp = (x+1)*i.width+y, dxm = (x-1)*i.width+y;
        final int fx1 = (i.img[dxp] & 0xFF) - (i.img[dxm] & 0xFF);
        final int dyp = x*i.width+(y+1), dym = x*i.width+(y-1);
        final int fy1 = (i.img[dyp] & 0xFF) - (i.img[dym] & 0xFF);
        partiald.get("fx2").setPixel(x, y, Math.pow(fx1, 2));
        partiald.get("fy2").setPixel(x, y, Math.pow(fy1, 2));
        partiald.get("fxy").setPixel(x, y, (double)(fx1 * fy1));
      }
    }

    gaussian.put("fx2", gaussianSmooth(partiald.get("fx2"), sigma));
    gaussian.put("fy2", gaussianSmooth(partiald.get("fy2"), sigma));
    gaussian.put("fxy", gaussianSmooth(partiald.get("fxy"), sigma));

    for (int x: IntStream.range(0, i.height).toArray()) {
      for (int y: IntStream.range(0, i.width).toArray()) {
        final double trace = gaussian.get("fx2").getPixel(x, y) + gaussian.get("fy2").getPixel(x, y);
        final double det = gaussian.get("fx2").getPixel(x, y)*gaussian.get("fy2").getPixel(x, y) - Math.pow(gaussian.get("fxy").getPixel(x, y), 2);
        i.img[x*i.width+y] = (byte)((det - KAPPA*Math.pow(trace, 2)) > threshold ? 255 : 0);
      }
    }
  }

  public static void main(String[] args) {
    new P2_3();
  }
}
