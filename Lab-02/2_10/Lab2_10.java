/**

In this task you will implement the method laplacianFilter of the class Lab2_10 which applies the Laplacian filter on the image.

Implement the isotropic mask for rotations in  increments of 45 degrees with a positive weight at the center. Clip all values to be within 0 to 255. 

The expected output is provided in the file solution.png.

You may use the following command to check if your output is identical to ours. 

cmp solution.png out.png

If this command has no output, it implies that your solution has produced the same file as ours.

**/

import java.util.Scanner;

public class Lab2_10 {
  public Lab2_10() {
    Img img = new Img("Fig0338.png");
    laplacianFilter(img);
    img.save();
  }

  public void laplacianFilter(Img i) {
    // Your code here

    final int SIZE = 3;
    int[] mask = new int[]{-1, -1, -1, -1, 8, -1, -1, -1, -1}; 

    byte[] iClone = new byte[i.width * i.height];
    for (int x=0; x<i.height; x++) {
      for (int y=0; y<i.width; y++) {
        iClone[x*i.width+y] = i.img[x*i.width+y];
      }
    }

    int offset = SIZE/2;

    for (int x = offset; x < i.height-offset; x++) {
      for (int y = offset; y < i.width-offset; y++) {
        int ptrMask = 0;
        double intensity = 0;
        for (int x_ = x-offset; x_ <= x+offset; x_++) {
          for (int y_ = y-offset; y_ <= y+offset; y_++) {
            intensity += (int)(i.img[x_*i.width+y_] & 0xFF) * (double)mask[ptrMask++];
          }
        }
        iClone[x*i.width+y] = (byte)((int)Math.min(255, Math.max(intensity, 0)));
      }
    }

    i.img = iClone;
  }

  public static void main(String[] args) {
    new Lab2_10();
  }
}
