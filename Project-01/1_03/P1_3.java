/**

  In this task 3 of project 1 you will design an appropriate filter and apply it to the car image to attenuate the impulse-like bursts in the image. You may reuse existing code from your tasks 1 and 2. 

Hint: Note that the spatial resolution of the car.png is not a power of 2, i.e., it is not of the form 2^m x 2^n. You should create a new suitable image with padded black pixels by changing code in the constructor of the class P1_3. 

To avoid reverse engineering, we do not provide a sample solution for this task.

 **/

import java.time.Instant;
import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class P1_3 {
  private int DIMN;   // Padding Dimension
  private int HEIGHT; // Original Height
  private int WIDTH;  // Original Width

  public P1_3() {
    // Change this code. 
    Img img = new Img("car.png");
    this.DIMN = (int)Math.pow(2, Math.ceil(Math.log(Math.max(img.height, img.width))/Math.log(2))+1);

    Instant start = Instant.now();

    // Start Padding
    byte[] padImage = new byte[this.DIMN * this.DIMN];
    for (int x=0; x<this.DIMN; x++)
      for (int y=0; y<this.DIMN; y++)
        if (x < img.height && y < img.width)
          padImage[x*this.DIMN+y] = img.img[x*img.width+y];
        else
          padImage[x*this.DIMN+y] = (byte)0;
    img.img = padImage;
    this.HEIGHT = img.height;
    this.WIDTH = img.width;
    img.height = this.DIMN;
    img.width = this.DIMN;
    // Finish Padding

    filterImage(img);

    // Start Unpadding
    byte[] finalImage = new byte[this.HEIGHT * this.WIDTH];
    for (int x=0; x<this.HEIGHT; x++)
      for (int y=0; y<this.WIDTH; y++)
        finalImage[x*this.WIDTH+y] = img.img[x*this.DIMN+y];
    img.img = finalImage;
    img.height = this.HEIGHT;
    img.width = this.WIDTH;
    // Finish Unpadding

    Instant stop = Instant.now();

    System.out.println("Elapsed time: "+Duration.between(start, stop).getSeconds()+"s");
    img.save();
  }

  public Complex conjugate(Complex x) {
    return new Complex(x.r, -x.i);
  }

  private int bitReverse(int j, int levels) {
    int j2;
    int j1 = j;
    int k = 0;
    while (levels-- > 0) {
      j2 = j1 / 2;
      k = 2 * k + j1 - 2 * j2;
      j1 = j2;
    }
    return k;
  }

  public Complex[] fft1d(final Complex[] inputComplex) {
    int n = inputComplex.length;

    double ld = Math.log(n) / Math.log(2.0);

    if (((int) ld) - ld != 0) {
      System.out.println("The number of elements is not a power of 2.");
      return null;
    }

    int nu = (int) ld;
    int n2 = n / 2;
    int nu1 = nu - 1;
    Complex[] xComplex = new Complex[n];
    double p, arg, c, s;
    Complex tComplex;

    double constant;
    if (true)
      constant = -2 * Math.PI;
    else
      constant = 2 * Math.PI;

    for (int i = 0; i < n; i++) {
      xComplex[i] = new Complex(inputComplex[i].r, inputComplex[i].i);
    }

    int k = 0;
    for (int l = 1; l <= nu; l++) {
      while (k < n) {
        for (int i = 1; i <= n2; i++) {
          p = bitReverse(k >> nu1, nu);
          arg = constant * p / n;
          c = Math.cos(arg);
          s = Math.sin(arg);
          tComplex = new Complex(xComplex[k+n2].r * c + xComplex[k+n2].i * s, xComplex[k+n2].i * c - xComplex[k+n2].r * s);
          xComplex[k+n2] = new Complex(xComplex[k].r, xComplex[k].i);
          xComplex[k+n2].minus(tComplex);
          xComplex[k].plus(tComplex);
          k++;
        }
        k += n2;
      }
      k = 0;
      nu1--;
      n2 /= 2;
    }

    k = 0;
    int r;
    while (k < n) {
      r = bitReverse(k, nu);
      if (r > k) {
        tComplex = new Complex(xComplex[k].r, xComplex[k].i);
        xComplex[k] = new Complex(xComplex[r].r, xComplex[r].i);
        xComplex[r] = new Complex(tComplex.r, tComplex.i);
      }
      k++;
    }
    return xComplex;
  }

  public Complex[][] fastFourierTransform(Img i) {
    // Change this code
    Complex[][] f2d_prime = new Complex[i.height][i.width];
    Complex[][] f2d_clone = new Complex[i.height][i.width];
    for (int x=0; x<i.height; x++) {
      for (int y=0; y<i.width; y++) {
        f2d_clone[x][y] = new Complex(i.img[x*i.width+y] & 0xFF, 0);
      }
    }

    for (int x=0; x<i.height; x++) {
      Complex[] preIFFT = new Complex[i.height];
      for (int y=0; y<i.width; y++)
        preIFFT[y] = f2d_clone[x][y];
      Complex[] postIFFT = fft1d(preIFFT);
      for (int y=0; y<i.width; y++)
        f2d_clone[x][y] = postIFFT[y];
    }

    for (int x=0; x<i.height; x++) {
      Complex[] preIFFT = new Complex[i.height];
      for (int y=0; y<i.width; y++)
        preIFFT[y] = f2d_clone[y][x];
      Complex[] postIFFT = fft1d(preIFFT);
      for (int y=0; y<i.width; y++)
        f2d_clone[y][x] = postIFFT[y];
    }

    for (int x=0; x<i.height; x++) {
      for (int y=0; y<i.width; y++) {
        f2d_prime[x][y] = new Complex();
        f2d_prime[x][y].plus(f2d_clone[(x+i.height/2)%i.height][(y+i.width/2)%i.width]);
      }
    }

    return f2d_prime;
  }

  private void inverseFastFourierTransform(Complex[][] F, Img i) {
    //Your code here
    Complex[][] fConjugate2d = new Complex[i.height][i.width];
    Complex[] fConjugate1r = new Complex[i.width];
    Complex[] fConjugate1c = new Complex[i.height];

    for (int u=0; u<i.height; u++) {
      for(int v=0; v<i.width; v++) {
        fConjugate1r[v] = conjugate(F[u][v]);
      }
      fConjugate1r = fft1d(fConjugate1r);
      for (int y=0; y<i.width; y++) {
        fConjugate2d[u][y] = fConjugate1r[y];
      }
    }

    for (int y=0; y<i.width; y++) {
      for (int u=0; u<i.height; u++) {
        fConjugate1c[u] = fConjugate2d[u][y];
      }
      fConjugate1c = fft1d(fConjugate1c);
      for (int x=0; x<i.height; x++) {
        fConjugate1c[x].div(i.width*i.height);
        fConjugate1c[x].mul(Math.pow(-1, x+y));
        i.img[x*i.width+y] = (byte)Math.min(Math.max(fConjugate1c[x].r, 0), 255);
      }
    }
  }

  public void filterImage(Img i) {
    Complex[][] F = fastFourierTransform(i);

    // Your code here
    final ArrayList<ConcurrentHashMap<String, Integer>> pivot = new ArrayList<ConcurrentHashMap<String, Integer>>() {{
      add(new ConcurrentHashMap<String, Integer>() {{ this.put("uk",  90); this.put("vk", 168); }});
      add(new ConcurrentHashMap<String, Integer>() {{ this.put("uk",  84); this.put("vk", 338); }});
      add(new ConcurrentHashMap<String, Integer>() {{ this.put("uk", 176); this.put("vk", 166); }});
      add(new ConcurrentHashMap<String, Integer>() {{ this.put("uk", 168); this.put("vk", 338); }});
    }};

    final double D0 = 40;
    final double n = 4.0;

    for (int u=0; u<i.height; u++) {
      for (int v=0; v<i.width; v++) {
        final int _u = u;
        final int _v = v;
        double Huv = pivot.stream().reduce(1.0, (product, p) -> {
          final double Dpkuv = Math.sqrt(Math.pow(_u-this.DIMN/2-(p.get("uk")-this.DIMN/2), 2) + Math.pow(_v-this.DIMN/2-(p.get("vk")-this.DIMN/2), 2));
          final double Dnkuv = Math.sqrt(Math.pow(_u-this.DIMN/2+(p.get("uk")-this.DIMN/2), 2) + Math.pow(_v-this.DIMN/2+(p.get("vk")-this.DIMN/2), 2));
          return product * Math.pow(1+Math.pow(D0/Dpkuv, n), -1) * Math.pow(1+Math.pow(D0/Dnkuv, n), -1);
        }, Double::sum);
        F[u][v].mul(Huv);
      }
    }

    inverseFastFourierTransform(F, i);
  }

  public static void main(String[] args) {
    new P1_3();
  }
}
