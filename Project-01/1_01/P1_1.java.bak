/**

  In this task 1 of project 1 you will implement the fast Fourier transform and change the image to the Fourier spectrum in the method fourierSpectrum(). Your task is to implement the missing code in the method fastFourierTransform(). The implementation details of the FFT can be obtained in section 4.11 of our Textbook. 

  Use the log transformation and ensure that all values are in the range 0 ... 255. 

  You may use methods declared in the class Complex.java for your convenience and you may add new methods to that class if necessary. 

  The solution files are provided for qualitative comparison. Output could be different because of differences in floating point arithmetic and differences in the way the rescaling is performed.

  For your reference, we are able generate the Fourier spectrum of the file rectangle1024.png in < 1 seconds. 

 **/

import java.time.Instant;
import java.time.Duration;

public class P1_1 {
  public P1_1() {
    Img img = new Img("ic512.png");
    Instant start = Instant.now();
    fourierSpectrum(img);
    Instant stop = Instant.now();
    System.out.println("Elapsed time: "+Duration.between(start, stop).getSeconds()+"s");
    img.save();
  }

  public void fourierSpectrum(Img i) {
    Complex[] F = fastFourierTransform(i);
    double max = Double.NEGATIVE_INFINITY;
    for (int x=0; x<F.length; x++)
      max = Math.max(F[x].getNorm(), max);
    for (int x=0; x<i.img.length; x++)
      i.img[x] = (byte)(255 / Math.log(256)*Math.log(255/max*F[x].getNorm()+1));
  }

  public Complex[] fastFourierTransform(Img i) {
    // Change this code
    Complex[] Fxv = new Complex[i.width*i.height];
    for (int v=0; v<i.width; v++) {
      for (int x=0; x<i.height; x++) {
        Fxv[x*i.width+v] = new Complex();
        for (int y=0; y<i.width; y++) {
          double f = (double)(i.img[x*i.width+y] & 0xFF) * Math.pow(-1, y+x);
          double theta = -2 * Math.PI * (v*y/(double)i.width);
          Fxv[x*i.width+v].plus(new Complex(Math.cos(theta) * f, Math.sin(theta) * f));
        }
      }
    }

    Complex[] Fuv = new Complex[i.width*i.height];
    for (int u=0; u<i.height; u++) {
      for (int v=0; v<i.width; v++) {
        Fuv[u*i.width+v] = new Complex();
        for (int x=0; x<i.height; x++) {
          double theta = -2 * Math.PI * (u*x/(double)i.height);
          Complex cuv = new Complex(Math.cos(theta), Math.sin(theta));
          cuv.mul(Fxv[x*i.width+v]);
          Fuv[u*i.width+v].plus(cuv);
        }
      }
    }
    return Fuv;
  }

  public static void main(String[] args) {
    new P1_1();
  }
}
