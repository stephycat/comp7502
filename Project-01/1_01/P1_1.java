/**

  In this task 1 of project 1 you will implement the fast Fourier transform and change the image to the Fourier spectrum in the method fourierSpectrum(). Your task is to implement the missing code in the method fastFourierTransform(). The implementation details of the FFT can be obtained in section 4.11 of our Textbook. 

  Use the log transformation and ensure that all values are in the range 0 ... 255. 

  You may use methods declared in the class Complex.java for your convenience and you may add new methods to that class if necessary. 

  The solution files are provided for qualitative comparison. Output could be different because of differences in floating point arithmetic and differences in the way the rescaling is performed.

  For your reference, we are able generate the Fourier spectrum of the file rectangle1024.png in < 1 seconds. 

 **/

import java.time.Instant;
import java.time.Duration;

public class P1_1 {
  public P1_1() {
    Img img = new Img("ic512.png");
    Instant start = Instant.now();
    fourierSpectrum(img);
    Instant stop = Instant.now();
    System.out.println("Elapsed time: "+Duration.between(start, stop).getSeconds()+"s");
    img.save();
  }

  public void fourierSpectrum(Img i) {
    Complex[] F = fastFourierTransform(i);
    double max = Double.NEGATIVE_INFINITY;
    for (int x=0; x<F.length; x++)
      max = Math.max(F[x].getNorm(), max);
    for (int x=0; x<i.img.length; x++)
      i.img[x] = (byte)(255 / Math.log(256)*Math.log(255/max*F[x].getNorm()+1));
  }

  private int bitReverse(int j, int levels) {
    int j2;
    int j1 = j;
    int k = 0;
    while (levels-- > 0) {
      j2 = j1 / 2;
      k = 2 * k + j1 - 2 * j2;
      j1 = j2;
    }
    return k;
  }

  public Complex[] fft1d(final Complex[] inputComplex) {
    int n = inputComplex.length;

    double ld = Math.log(n) / Math.log(2.0);

    if (((int) ld) - ld != 0) {
      System.out.println("The number of elements is not a power of 2.");
      return null;
    }

    int nu = (int) ld;
    int n2 = n / 2;
    int nu1 = nu - 1;
    Complex[] xComplex = new Complex[n];
    double p, arg, c, s;
    Complex tComplex;

    double constant;
    if (true)
      constant = -2 * Math.PI;
    else
      constant = 2 * Math.PI;

    for (int i = 0; i < n; i++) {
      xComplex[i] = new Complex(inputComplex[i].r, inputComplex[i].i);
    }

    int k = 0;
    for (int l = 1; l <= nu; l++) {
      while (k < n) {
        for (int i = 1; i <= n2; i++) {
          p = bitReverse(k >> nu1, nu);
          arg = constant * p / n;
          c = Math.cos(arg);
          s = Math.sin(arg);
          tComplex = new Complex(xComplex[k+n2].r * c + xComplex[k+n2].i * s, xComplex[k+n2].i * c - xComplex[k+n2].r * s);
          xComplex[k+n2] = new Complex(xComplex[k].r, xComplex[k].i);
          xComplex[k+n2].minus(tComplex);
          xComplex[k].plus(tComplex);
          k++;
        }
        k += n2;
      }
      k = 0;
      nu1--;
      n2 /= 2;
    }

    k = 0;
    int r;
    while (k < n) {
      r = bitReverse(k, nu);
      if (r > k) {
        tComplex = new Complex(xComplex[k].r, xComplex[k].i);
        xComplex[k] = new Complex(xComplex[r].r, xComplex[r].i);
        xComplex[r] = new Complex(tComplex.r, tComplex.i);
      }
      k++;
    }
    return xComplex;
  }

  public Complex[] fastFourierTransform(Img i) {
    // Change this code
    Complex[]   f1d_prime = new Complex[i.height*i.width];
    Complex[][] f2d_prime = new Complex[i.height][i.width];
    Complex[][] f2d_clone = new Complex[i.height][i.width];
    for (int x=0; x<i.height; x++) {
      for (int y=0; y<i.width; y++) {
        f2d_clone[x][y] = new Complex(i.img[x*i.width+y] & 0xFF, 0);
      }
    }

    for (int x=0; x<i.height; x++) {
      Complex[] preIFFT = new Complex[i.height];
      for (int y=0; y<i.width; y++)
        preIFFT[y] = f2d_clone[x][y];
      Complex[] postIFFT = fft1d(preIFFT);
      for (int y=0; y<i.width; y++)
        f2d_clone[x][y] = postIFFT[y];
    }

    for (int x=0; x<i.height; x++) {
      Complex[] preIFFT = new Complex[i.height];
      for (int y=0; y<i.width; y++)
        preIFFT[y] = f2d_clone[y][x];
      Complex[] postIFFT = fft1d(preIFFT);
      for (int y=0; y<i.width; y++)
        f2d_clone[y][x] = postIFFT[y];
    }

    for (int x=0; x<i.height; x++) {
      for (int y=0; y<i.width; y++) {
        f1d_prime[x*i.width+y] = new Complex();
        f1d_prime[x*i.width+y].plus(f2d_clone[(x+i.height/2)%i.height][(y+i.width/2)%i.width]);
        f2d_prime[x][y] = new Complex();
        f2d_prime[x][y].plus(f2d_clone[(x+i.height/2)%i.height][(y+i.width/2)%i.width]);
      }
    }

    return f1d_prime;
  }

  public static void main(String[] args) {
    new P1_1();
  }
}
