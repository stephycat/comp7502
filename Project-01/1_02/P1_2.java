/**

  In this task 2 of project 1 you will implement the inverse fast Fourier transform and perform second order (n=2) ButterWorth low pass filtering in the frequency domain. You should reuse your implementation of the fast Fourier transform from the previous task of this project (P1_1).

  In the filterImage() method add your code for the second order (n=2) ButterWorth low pass filtering.

  Implement the inverse fast Fourier transform in the method inverseFourierTransform().

  You may use methods declared in the class Complex.java for your convenience.

  The solution file is provided for qualitative comparison. It was generated with d0=10, i.e., with the command

  java P1_2 10

  Output could be different because of differences in floating point arithmetic and differences in the way the rescaling is performed. 

 **/

import java.time.Instant;
import java.time.Duration;

public class P1_2 {
  public P1_2(double d0) {
    Img img = new Img("ic512.png");
    Instant start = Instant.now();
    filterImage(img, d0);
    Instant stop = Instant.now();
    System.out.println("Elapsed time: "+Duration.between(start, stop).getSeconds()+"s");
    img.save();
  }

  public Complex conjugate(Complex x) {
    return new Complex(x.r, -x.i);
  }

  private int bitReverse(int j, int levels) {
    int j2;
    int j1 = j;
    int k = 0;
    while (levels-- > 0) {
      j2 = j1 / 2;
      k = 2 * k + j1 - 2 * j2;
      j1 = j2;
    }
    return k;
  }

  public Complex[] fft1d(final Complex[] inputComplex) {
    int n = inputComplex.length;

    double ld = Math.log(n) / Math.log(2.0);

    if (((int) ld) - ld != 0) {
      System.out.println("The number of elements is not a power of 2.");
      return null;
    }

    int nu = (int) ld;
    int n2 = n / 2;
    int nu1 = nu - 1;
    Complex[] xComplex = new Complex[n];
    double p, arg, c, s;
    Complex tComplex;

    double constant;
    if (true)
      constant = -2 * Math.PI;
    else
      constant = 2 * Math.PI;

    for (int i = 0; i < n; i++) {
      xComplex[i] = new Complex(inputComplex[i].r, inputComplex[i].i);
    }

    int k = 0;
    for (int l = 1; l <= nu; l++) {
      while (k < n) {
        for (int i = 1; i <= n2; i++) {
          p = bitReverse(k >> nu1, nu);
          arg = constant * p / n;
          c = Math.cos(arg);
          s = Math.sin(arg);
          tComplex = new Complex(xComplex[k+n2].r * c + xComplex[k+n2].i * s, xComplex[k+n2].i * c - xComplex[k+n2].r * s);
          xComplex[k+n2] = new Complex(xComplex[k].r, xComplex[k].i);
          xComplex[k+n2].minus(tComplex);
          xComplex[k].plus(tComplex);
          k++;
        }
        k += n2;
      }
      k = 0;
      nu1--;
      n2 /= 2;
    }

    k = 0;
    int r;
    while (k < n) {
      r = bitReverse(k, nu);
      if (r > k) {
        tComplex = new Complex(xComplex[k].r, xComplex[k].i);
        xComplex[k] = new Complex(xComplex[r].r, xComplex[r].i);
        xComplex[r] = new Complex(tComplex.r, tComplex.i);
      }
      k++;
    }
    return xComplex;
  }

  public Complex[][] fastFourierTransform(Img i) {
    // Change this code
    Complex[][] f2d_prime = new Complex[i.height][i.width];
    Complex[][] f2d_clone = new Complex[i.height][i.width];
    for (int x=0; x<i.height; x++) {
      for (int y=0; y<i.width; y++) {
        f2d_clone[x][y] = new Complex(i.img[x*i.width+y] & 0xFF, 0);
      }
    }

    for (int x=0; x<i.height; x++) {
      Complex[] preIFFT = new Complex[i.height];
      for (int y=0; y<i.width; y++)
        preIFFT[y] = f2d_clone[x][y];
      Complex[] postIFFT = fft1d(preIFFT);
      for (int y=0; y<i.width; y++)
        f2d_clone[x][y] = postIFFT[y];
    }

    for (int x=0; x<i.height; x++) {
      Complex[] preIFFT = new Complex[i.height];
      for (int y=0; y<i.width; y++)
        preIFFT[y] = f2d_clone[y][x];
      Complex[] postIFFT = fft1d(preIFFT);
      for (int y=0; y<i.width; y++)
        f2d_clone[y][x] = postIFFT[y];
    }

    for (int x=0; x<i.height; x++) {
      for (int y=0; y<i.width; y++) {
        f2d_prime[x][y] = new Complex();
        f2d_prime[x][y].plus(f2d_clone[(x+i.height/2)%i.height][(y+i.width/2)%i.width]);
      }
    }

    return f2d_prime;
  }

  private void inverseFastFourierTransform(Complex[][] F, Img i) {
    //Your code here
    Complex[][] fConjugate2d = new Complex[i.height][i.width];
    Complex[] fConjugate1r = new Complex[i.width];
    Complex[] fConjugate1c = new Complex[i.height];

    for (int u=0; u<i.height; u++) {
      for(int v=0; v<i.width; v++) {
        fConjugate1r[v] = conjugate(F[u][v]);
      }
      fConjugate1r = fft1d(fConjugate1r);
      for (int y=0; y<i.width; y++) {
        fConjugate2d[u][y] = fConjugate1r[y];
      }
    }

    for (int y=0; y<i.width; y++) {
      for (int u=0; u<i.height; u++) {
        fConjugate1c[u] = fConjugate2d[u][y];
      }
      fConjugate1c = fft1d(fConjugate1c);
      for (int x=0; x<i.height; x++) {
        fConjugate1c[x].div(i.width*i.height);
        fConjugate1c[x].mul(Math.pow(-1, x+y));
        i.img[x*i.width+y] = (byte)Math.min(Math.max(fConjugate1c[x].r, 0), 255);
      }
    }
  }

  public void filterImage(Img i, double d0) {
    Complex[][] F = fastFourierTransform(i);
    // Your code here
    for (int u=0; u<i.height; u++) {
      for (int v=0; v<i.width; v++) {
        double Duv = Math.sqrt(Math.pow(u-i.height/2, 2) + Math.pow(v-i.width/2, 2));
        double Huv = Math.pow(1 + Math.pow(Duv/d0, 2*2), -1);
        F[u][v].mul(Huv);
      }
    }
    inverseFastFourierTransform(F, i);
  }

  public static void main(String[] args) {
    new P1_2(Double.parseDouble(args[0]));
  }
}
